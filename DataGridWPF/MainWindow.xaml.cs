﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace DataGridWPF
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public int globalImageWidth = (int)(SystemParameters.PrimaryScreenHeight) - 100;


        class TestRow   //klasa do losowych danych wpisywanych do datagrida
        {
            public int ID { get; set; }
            public char RandomChar { get; set; }
            public double RandomDouble { get; set; }
        }
        public MainWindow()
        {
            InitializeComponent();

            dataGrid1.ItemsSource = InstantiateRandomData();
            Test();
            
        }

        private void Test()
        {
            List<Point> points = CreateRandomPoints(10).ToList();
            TestImage testImage = new TestImage(points);
            ImageCanvas.Children.Add(testImage);

            //imageView.Source = loadBitmap(bitmap);
        }


        private List<TestRow> InstantiateRandomData()   //tworzenie losowych danych do wpisania do datagrida
        {
            List<TestRow> list = new List<TestRow>();
            Random rng = new Random();
            char[] chars =
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();

            for (int i = 0; i < 100000; i++)
            {

                list.Add(new TestRow
                {
                    ID = i,
                    RandomChar = chars[rng.Next(0, chars.Length)],
                    RandomDouble = rng.NextDouble()
                });
            }
            return list;
        }

        private IEnumerable<Point> CreateRandomPoints(uint n)
        {
            List<Point> list = new List<Point>();
            Random rng = new Random();

            for (int i = 0; i < n; i++)
            {
                list.Add(new Point
                {
                    X = rng.NextDouble() * globalImageWidth,
                    Y = rng.NextDouble() * globalImageWidth
                });
            }

            return list;
        }
    }
}
