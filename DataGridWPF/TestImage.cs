﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace DataGridWPF
{
    class TestImage : FrameworkElement
    {
        private readonly VisualCollection _children;


        public TestImage(List<Point> points)
        {
            _children = new VisualCollection(this);

            foreach(Point point in points)
            {
                _children.Add(CreateDrawingVisualEllipse(point, 20, 20));
            }
        }

        private DrawingVisual CreateDrawingVisualEllipse(Point middlePoint, double radiusX, double radiusY)
        {
            DrawingVisual drawingVisual = new DrawingVisual();
            DrawingContext drawingContext = drawingVisual.RenderOpen();
            drawingContext.DrawEllipse(Brushes.IndianRed, null, middlePoint, radiusX, radiusY);
            drawingContext.Close();

            return drawingVisual;
        }


        // Provide a required override for the VisualChildrenCount property.
        protected override int VisualChildrenCount => _children.Count;

        // Provide a required override for the GetVisualChild method.
        protected override Visual GetVisualChild(int index)
        {
            if (index < 0 || index >= _children.Count)
            {
                throw new ArgumentOutOfRangeException();
            }

            return _children[index];
        }
    }
}
